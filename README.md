# PostgreSQL Training Labs

## Prerequisites

Have installed docker or podman.

## Build

You can spin up with `docker-compose up|down` inside each `labs/<name>` folder or, use the `make` as follows:


```
make patroni-up
... do stuff
make patroni-down
```

