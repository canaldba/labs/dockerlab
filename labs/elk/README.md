# elk

[![Try in PWD](https://raw.githubusercontent.com/play-with-docker/stacks/master/assets/images/button.png)](https://labs.play-with-docker.com/?stack=https://gitlab.com/canaldba/labs/dockerlab/-/raw/master/labs/elk/docker-compose.yaml)

```
docker-compose up 
```

This should spin up a [Kibana](http://localhost:5601/app/kibana#) on port 5601.
