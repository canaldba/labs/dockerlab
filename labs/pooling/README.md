# pooling

This lab spins a pgbouncer pointing to a Postgres endpoint.

For accessing PgBouncer admin console:

```
psql -p 15555 -h localhost -Upostgres pgbouncer
```

To access the postgres locally:

```
psql -p 15555 -h localhost -Upostgres postgres
```