# PoC_Prometheus_Postgres

This repository allows to test exporters and alert manager for Prometheus integrated with Postgres. 


> Use official Docker images, avoid building images 
> Do not compile docker images unless strictly necessary


## HOWTO

```
docker-compose up postgres10
docker-compose up prometheus

...
docker-compose up postgres-exporter
```


## Query examples for envoy Network filter work

```
rate(envoy_postgresql_egress_postgresql_statements_select[1m]) or label_replace(rate(envoy_postgresql_egress_postgresql_statements_update[1m]),"distinct","foo","foo",".*")
```

```
rate(envoy_postgresql_egress_postgresql_transactions_commit[1m]) or label_replace(rate(envoy_postgresql_egress_postgresql_transactions_rollback[1m]), "distinct", "rollback", "rollback", ".*")
```