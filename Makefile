SHELL=/bin/bash
makePath := $(abspath .)

.PHONY: prometheus prom-down elk elk-down pooling pooling-down

# docker-compose exec postgres10 pgbench -i -Upostgres -s25 && \
# docker-compose exec postgres10 pgbench -Upostgres -n -c 30 -T 40 && \

prometheus:
	cd labs/prometheus && docker-compose up -d postgres10 && \
	docker-compose up -d prometheus && sleep 25 && \
	docker-compose exec postgres10 psql -Upostgres -c "create table test as select i from generate_series(1,100000) i(i)" && \
	docker-compose exec postgres10 psql -Upostgres -c "UPDATE test set i = i + 10 where i > 80000" && \
	docker-compose exec postgres10 psql -Upostgres -c "analyze" && \
	docker-compose up -d postgres-exporter

prom-down:
	cd labs/prometheus && docker-compose down && \
	rm -rf .data/ && rm -rf .volumes/

elk:
	cd labs/elk && docker-compose up

elk-down:
	cd labs/elk && docker-compose down

pooling:
	cd labs/pooling && docker-compose up

pooling-down:
	cd labs/pooling && docker-compose down
